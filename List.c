#include "List.h"

// * STATIC
bool matchAddress(item i, item ID) {
    return i == ID;
}

bool addLinkInOrderR(link list, link l, bool (*minor)(item a, item l, item args), item args) {
    if (list == NULL) { // Interruzione in caso di ultimo elemento
        return true;    // Chiedo alla ricorsione precedente di inserire e basta
    }
    if (list->Item != NULL) {                   // Se non mi trovo sulla HEAD
        if (minor(l->Item, list->Item, args)) { // Se l'elemento da inserire viene prima dell'elemento confrontato in lista
            return true;                        // Do l'ok alla ricorsione sul livello precedente per inserirlo prima di quello esplorato
        }
    }
    if (addLinkInOrderR(list->Next, l, minor, args)) { // Se la ricorsione successiva conferma l'inserimento
        l->Next    = list->Next;                       // Aggiorno il next del nuovo elemento
        list->Next = l;                                // Aggiorno il next dell'elemento precedente a quello "sostituito"
    }
    return false; // Finisco qui solo in caso di errore
}
// * END OF STATIC

/* Aggiunge un item alla lista in ordine
   La funzione minor restituisce se a viene prima di l secondo l'ordine crescente e prende in input:
   - item a (già presente in lista)
   - item l (l'item da aggiungere)
   - item args (eventuali argomenti della funzione) */
void addItemInOrder(link HEAD, item i, bool (*minor)(item a, item l, item args), item args) {
    addLinkInOrder(HEAD, newLink(i), minor, args);
}

/* Aggiunge un link alla lista in ordine
   La funzione minor restituisce se a viene prima di l secondo l'ordine crescente e prende in input:
   - item a (già presente in lista)
   - item l (l'item del link da aggiungere)
   - item args (eventuali argomenti della funzione) */
void addLinkInOrder(link HEAD, link l, bool (*minor)(item a, item l, item args), item args) {
    if (HEAD == NULL) {
        return;
    }
    HEAD = getHead(HEAD);                  // Risalgo alla HEAD
    addLinkInOrderR(HEAD, l, minor, args); // Chiamo la funzione ricorsiva
}

// Conta gli elementi in una lista
unsigned int countItemsList(link list) {
    if (list == NULL) { // Interruzione per lista non valida
        return 0;
    }

    list               = getHead(list); // trovo la head
    unsigned int count = 0;
    while (getNext(&list)) { // Sinché ho l'elemento successivo
        count++;
    }
    return count;
}

// Conta gli elementi in una lista
unsigned int countValidItemsList(link list, bool (*valid)(item i, item args), item args) {
    if (list == NULL) { // Interruzione per lista non valida
        return 0;
    }
    list               = getHead(list); // trovo la head
    unsigned int count = 0;
    while (getNext(&list)) {              // Sinché ho l'elemento successivo
        if ((*valid)(list->Item, args)) { // Se l'elemento è valido
            count++;
        }
    }
    return count;
}

// Elimina ogni nodo di una lista a partire dalla head
void freeListFromHead(link list) {
    if (list->Next != NULL) {
        freeListFromHead(list->Next);
    }
    free(list);
}

// Elimina ogni nodo di una lista a partire dalla tail
void freeListFromTail(link list) {
    if (list->Previous != NULL) {
        freeListFromTail(list->Previous);
    }
    free(list);
}

// Elimina ogni nodo della lista dalla memoria
void freeList(link list) {
    link next = list->Next;
    freeListFromTail(list);
    if (next != NULL) {
        freeListFromHead(next);
    }
}

// Restituisce la testa di una lista
link getHead(link l) {
    if (l->Previous == NULL) {
        return l;
    }
    return getHead(l->Previous);
}

// Restituisce l'iesimo elemento della lista (0 è la HEAD), se non esiste restituisce NULL
item getListItemByIndex(link HEAD, unsigned int i) {
    HEAD               = getHead(HEAD);
    unsigned int index = 0;
    while (getNext(&HEAD) && index < i) { // Leggo sinché possibile o sino a trovare l'iesimo elemento
        index++;
    }
    return index == i ? HEAD->Item : NULL;
}

// Aggiorna il link con l'elemento successivo in lista, se sono sull'ultimo elemento restituisce false
bool getNext(link *l) {
    link list = *l;
    if (list == NULL) { // In caso di elemento nullo
        return false;
    }
    if (list->Next == NULL) { // In caso di elemento inesistente
        return false;
    }
    *l = list->Next; // Aggiorno l'elemento
    return true;
}

// Aggiorna il link con l'elemento precedente in lista, se sono sul primo elemento restituisce false
bool getPrevious(link *l) {
    link list = *l;
    if (list == NULL) { // In caso di elemento nullo
        return false;
    }
    if (list->Previous == NULL) { // In caso di elemento inesistente
        return false;
    }
    *l = list->Previous; // Aggiorno l'elemento
    return true;
}

// Restituisce la coda di una lista
link getTail(link l) {
    if (l->Next == NULL) {
        return l;
    }
    return getTail(l->Next);
}

// Crea un nodo di lista
link newLink(item i) {
    link l      = (link)malloc(sizeof(struct List));
    l->Item     = i;
    l->Previous = NULL;
    l->Next     = NULL;
    return l;
}

// Estrae e restituisce l'ultimo item da una lista
item popItem(link list) {
    // Salvo i dati necessari
    link tail    = getTail(list);
    item temp    = tail->Item;
    link newTail = tail->Previous;

    // Rimpiazzo la coda
    newTail->Next = NULL;
    free(tail);

    return temp;
}

// Estrae e restituisce l'ultimo link da una lista
item popLink(link list) {
    // Salvo i dati necessari
    link tail    = getTail(list);
    item temp    = tail->Item;
    link newTail = tail->Previous;

    // Rimpiazzo la coda
    newTail->Next = NULL;

    return tail;
}

// Estrae e restituisce l'item in cima alla lista
item pullItem(link list) {
    // Salvo i dati necessari
    link head    = getHead(list);
    item temp    = head->Item;
    link newHead = head->Next;

    // Rimpiazzo la testa
    newHead->Previous = NULL;
    free(head);

    return temp;
}

// Estrae e restituisce il link in cima alla lista
item pullLink(link list) {
    // Salvo i dati necessari
    link head    = getHead(list);
    item temp    = head->Item;
    link newHead = head->Next;

    // Rimpiazzo la testa
    newHead->Previous = NULL;

    return head;
}

// Aggiunge un link in cima alla lista
void pushLink(link list, link l) {
    link head      = getHead(list);
    head->Previous = l;
    l->Next        = head;
}

// Aggiunge un item a fine lista
void pushItem(link list, item i) {
    link l = newLink(i);
    pushLink(list, l);
}

// Aggiunge un link a fine lista
void putLink(link list, link l) {
    link tail   = getTail(list);
    l->Previous = tail;
    tail->Next  = l;
}

// Aggiunge un item a fine lista
void putItem(link list, item i) {
    link l = newLink(i);
    putLink(list, l);
}

/* Ricerca un item tramite un campo identificativo ed una funzione di match partendo dal link sino alla tail, restitusice NULL se non è stato trovato
   matchID = Funzione che restituisce true se l'elemento corretto combacia con l'identificativo */
item searchByIDfromHead(link list, item ID, bool (*matchID)(item i, item ID)) {
    if (list->Next == NULL) { // Se mi trovo sull'ultimo elemento
        return (*matchID)(list->Item, ID) ? list->Item : NULL;
    }
    return (*matchID)(list->Item, ID) ? list->Item : searchByID(list->Next, ID, matchID);
}

/* Ricerca un item tramite un campo identificativo ed una funzione di match partendo dal link sino alla head, restitusice NULL se non è stato trovato
   matchID = Funzione che restituisce true se l'elemento corretto combacia con l'identificativo */
item searchByIDfromTail(link list, item ID, bool (*matchID)(item i, item ID)) {
    if (list->Previous == NULL) { // Se mi trovo sul primo elemento
        return (*matchID)(list->Item, ID) ? list->Item : NULL;
    }
    return (*matchID)(list->Item, ID) ? list->Item : searchByIDfromTail(list->Previous, ID, matchID);
}

/* Ricerca un item tramite un campo identificativo ed una funzione di match, restitusice NULL se non è stato trovato
   matchID = Funzione che restituisce true se l'elemento corretto combacia con l'identificativo */
item searchByID(link list, item ID, bool (*matchID)(item i, item ID)) {
    link top = searchByIDfromTail(list, ID, matchID);
    return top == NULL ? searchByIDfromHead(list, ID, matchID) : top;
}

item searchByPointer(link list, item PointerValue) {
    return searchByID(list, PointerValue, (void *)&matchAddress);
}

// Restituisce una lista con solo gli elementi validi
link validItemsList(link list, bool (*valid)(item i, item args), item args) {
    if (list == NULL) { // Interruzione per lista non valida
        return NULL;
    }
    list         = getHead(list); // trovo la head
    link newList = newLink(NULL);
    while (getNext(&list)) {              // Sinché ho l'elemento successivo
        if ((*valid)(list->Item, args)) { // Se l'elemento è valido
            putItem(newList, list->Item);
        }
    }
    return newList;
}

// Restituisce una lista con solo gli elementi validi
link validItemsListWithCount(link list, bool (*valid)(item i, item args), item args, unsigned int *numberOfItems) {
    if (list == NULL) { // Interruzione per lista non valida
        return NULL;
    }
    list         = getHead(list); // trovo la head
    link newList = newLink(NULL);

    if (numberOfItems == NULL) {
        while (getNext(&list)) {              // Sinché ho l'elemento successivo
            if ((*valid)(list->Item, args)) { // Se l'elemento è valido
                putItem(newList, list->Item);
            }
        }
    } else {
        *numberOfItems = 0;
        while (getNext(&list)) {              // Sinché ho l'elemento successivo
            if ((*valid)(list->Item, args)) { // Se l'elemento è valido
                putItem(newList, list->Item);
                *numberOfItems++;
            }
        }
    }

    return newList;
}