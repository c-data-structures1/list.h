#ifndef LIST_H
#define LIST_H

// * DNT
#include <stdbool.h>
#include <stdlib.h>

#ifndef ITEM_DEF
#define ITEM_DEF
typedef void *item;
#endif // ! ITEM_DEF

typedef struct List *link;
struct List {
    item Item;
    link Previous;
    link Next;
};
// * EDNT

link validItemsListWithCount(link list, bool (*valid)(item i, item args), item args, unsigned int *numberOfItems);
link validItemsList(link list, bool (*valid)(item i, item args), item args);
item searchByPointer(link list, item PointerValue);
item searchByIDfromTail(link list, item ID, bool (*matchID)(item i, item ID));
item searchByID(link list, item ID, bool (*matchID)(item i, item ID));
item searchByIDfromHead(link list, item ID, bool (*matchID)(item i, item ID));
void putItem(link list, item i);
void putLink(link list, link l);
void pushItem(link list, item i);
void pushLink(link list, link l);
item pullLink(link list);
item pullItem(link list);
item popLink(link list);
item popItem(link list);
link getTail(link l);
bool getPrevious(link *l);
item getListItemByIndex(link HEAD, unsigned int i);
void freeList(link list);
void freeListFromTail(link list);
void freeListFromHead(link list);
unsigned int countValidItemsList(link list, bool (*valid)(item i, item args), item args);
bool getNext(link *l);
unsigned int countItemsList(link list);
link getHead(link l);
link newLink(item i);
void addLinkInOrder(link HEAD, link l, bool (*minor)(item a, item l, item args), item args);
void addItemInOrder(link HEAD, item i, bool (*minor)(item a, item l, item args), item args);
bool addLinkInOrderR(link list, link l, bool (*minor)(item a, item l, item args), item args);
bool matchAddress(item i, item ID);

#endif // ! LIST_H
